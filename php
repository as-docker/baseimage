FROM amsdard.io/baseimage/base

ARG PHP=7.1

RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
RUN apt-get update
RUN apt-get -y --allow-unauthenticated install php${PHP} \
    php${PHP}-curl php${PHP}-json php${PHP}-mysqlnd php${PHP}-mcrypt \
    php${PHP}-mysql php${PHP}-pgsql php${PHP}-mongo php${PHP}-redis \
    php${PHP}-gd php${PHP}-imagick php${PHP}-imap php${PHP}-memcache php${PHP}-xmlrpc \
    php${PHP}-bcmath php${PHP}-zip \
    php${PHP}-mbstring php${PHP}-soap php${PHP}-xml

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/sbin/my_init"]
