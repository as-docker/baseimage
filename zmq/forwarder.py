import zmq

def main():
    try:
        context = zmq.Context()

        # Socket facing clients
        frontend = context.socket(zmq.SUB)
        frontend.bind("tcp://*:5551")
        frontend.setsockopt(zmq.SUBSCRIBE, b'')
        print("Listening for published messages on port 5551")

        # Socket facing services
        backend = context.socket(zmq.PUB)
        backend.bind("tcp://*:5550")
        print("Forwarding messages to subscribers on port 5550")

        zmq.device(zmq.FORWARDER, frontend, backend)

    finally:
        frontend.close()
        backend.close()
        context.term()


if __name__ == "__main__":
    main()